v {xschem version=3.1.0 file_version=1.2 
}
G {}
K {}
V {}
S {}
E {}
N 170 -300 190 -300 {
lab=CNT0}
N 490 -270 490 -220 {
lab=EN0}
N 500 -270 500 -250 {
lab=VSS}
N 500 -180 500 -160 {
lab=VDD}
N 500 -100 500 -80 {
lab=VSS}
N 490 -100 490 -60 {
lab=ENB0}
N 450 -130 460 -130 {
lab=B}
N 520 -300 550 -300 {
lab=VDD}
N 550 -300 550 -190 {
lab=VDD}
N 520 -130 550 -130 {
lab=VDD}
N 450 -300 460 -300 {
lab=A}
N 490 -360 490 -330 {
lab=ENB0}
N 500 -350 500 -330 {
lab=VDD}
N 490 -370 490 -360 {
lab=ENB0}
N 500 -370 500 -350 {
lab=VDD}
N 490 -390 490 -370 {
lab=ENB0}
N 490 -220 490 -160 {
lab=EN0}
N 500 -200 500 -180 {
lab=VDD}
N 500 -250 500 -230 {
lab=VSS}
N 500 -230 500 -220 {
lab=VSS}
N 550 -190 550 -130 {
lab=VDD}
N 220 -370 220 -320 {
lab=VDD}
N 220 -280 220 -220 {
lab=VSS}
N 330 -280 330 -220 {
lab=VSS}
N 330 -370 330 -320 {
lab=VDD}
N 250 -300 300 -300 {
lab=ENB0}
N 360 -300 370 -300 {
lab=EN0}
N 550 -210 700 -210 {
lab=VDD}
N 760 -40 800 -40 {
lab=OUT}
N 800 -210 800 -90 {
lab=OUT}
N 760 -210 800 -210 {
lab=OUT}
N 670 -40 700 -40 {
lab=C}
N 740 -260 740 -240 {
lab=VDD}
N 740 -280 740 -260 {
lab=VDD}
N 730 -100 730 -70 {
lab=EN1}
N 740 -90 740 -70 {
lab=VDD}
N 730 -110 730 -100 {
lab=EN1}
N 740 -110 740 -90 {
lab=VDD}
N 730 -130 730 -110 {
lab=EN1}
N 800 -90 800 -40 {
lab=OUT}
N 740 -10 740 10 {
lab=VSS}
N 740 -180 740 -160 {
lab=VSS}
N 730 -180 730 -140 {
lab=EN1}
N 730 -140 730 -130 {
lab=EN1}
N 170 -110 190 -110 {
lab=CNT1}
N 220 -180 220 -130 {
lab=VDD}
N 220 -90 220 -30 {
lab=VSS}
N 330 -90 330 -30 {
lab=VSS}
N 330 -180 330 -130 {
lab=VDD}
N 250 -110 300 -110 {
lab=ENB1}
N 360 -110 370 -110 {
lab=EN1}
N 730 -10 730 30 {
lab=ENB1}
N 730 -300 730 -240 {
lab=ENB1}
C {devices/iopin.sym} 90 -340 2 0 {name=p1 lab=CNT0}
C {devices/iopin.sym} 90 -300 2 0 {name=p2 lab=A}
C {devices/iopin.sym} 90 -280 2 0 {name=p3 lab=B}
C {devices/iopin.sym} 90 -190 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 90 -170 2 0 {name=p6 lab=VSS}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 200 -280 0 0 {name=inv1
}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 310 -280 0 0 {name=inv2
}
C {tgate_gen/tgate_templates_xschem/tgate/tgate.sym} 450 -260 0 0 {name=tgate1}
C {tgate_gen/tgate_templates_xschem/tgate/tgate.sym} 450 -90 0 0 {name=tgate2
}
C {devices/lab_pin.sym} 370 -300 2 0 {name=l2 sig_type=std_logic lab=EN0}
C {devices/lab_pin.sym} 170 -300 0 0 {name=l4 sig_type=std_logic lab=CNT0}
C {devices/lab_pin.sym} 450 -300 0 0 {name=l8 sig_type=std_logic lab=A}
C {devices/lab_pin.sym} 450 -130 0 0 {name=l9 sig_type=std_logic lab=B}
C {devices/lab_pin.sym} 500 -200 2 0 {name=l10 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 500 -220 2 0 {name=l11 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 500 -370 2 0 {name=l12 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 500 -80 2 0 {name=l13 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 490 -390 2 0 {name=l14 sig_type=std_logic lab=ENB0}
C {devices/lab_pin.sym} 490 -60 2 0 {name=l15 sig_type=std_logic lab=ENB0}
C {devices/lab_pin.sym} 490 -220 0 0 {name=l16 sig_type=std_logic lab=EN0}
C {devices/lab_pin.sym} 800 -130 2 0 {name=l17 sig_type=std_logic lab=OUT}
C {devices/lab_pin.sym} 270 -300 3 0 {name=l3 sig_type=std_logic lab=ENB0}
C {devices/lab_pin.sym} 220 -370 0 0 {name=p7 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 220 -220 0 0 {name=p8 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 330 -220 0 0 {name=p9 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 330 -370 0 0 {name=p10 sig_type=std_logic lab=VDD}
C {tgate_gen/tgate_templates_xschem/tgate/tgate.sym} 690 -170 0 0 {name=tgate3}
C {tgate_gen/tgate_templates_xschem/tgate/tgate.sym} 690 0 0 0 {name=tgate4}
C {devices/lab_pin.sym} 670 -40 0 0 {name=l5 sig_type=std_logic lab=C}
C {devices/lab_pin.sym} 740 -280 2 0 {name=l6 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 740 -110 2 0 {name=l7 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 740 10 2 0 {name=l18 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 740 -160 2 0 {name=l19 sig_type=std_logic lab=VSS}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 200 -90 0 0 {name=inv3
}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 310 -90 0 0 {name=inv4
}
C {devices/lab_pin.sym} 270 -110 3 0 {name=l20 sig_type=std_logic lab=ENB1}
C {devices/lab_pin.sym} 370 -110 2 0 {name=l21 sig_type=std_logic lab=EN1}
C {devices/lab_pin.sym} 170 -110 0 0 {name=l22 sig_type=std_logic lab=CNT1}
C {devices/lab_pin.sym} 220 -180 0 0 {name=p11 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 220 -30 0 0 {name=p12 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 330 -30 0 0 {name=p13 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 330 -180 0 0 {name=p14 sig_type=std_logic lab=VDD}
C {devices/iopin.sym} 90 -320 2 0 {name=p15 lab=CNT1}
C {devices/iopin.sym} 90 -240 2 0 {name=p16 lab=OUT}
C {devices/iopin.sym} 90 -260 2 0 {name=p4 lab=C}
C {devices/lab_pin.sym} 730 -130 0 0 {name=l1 sig_type=std_logic lab=EN1}
C {devices/lab_pin.sym} 730 30 2 0 {name=l23 sig_type=std_logic lab=ENB1}
C {devices/lab_pin.sym} 730 -300 2 0 {name=l24 sig_type=std_logic lab=ENB1}
C {devices/lab_pin.sym} 580 -210 2 0 {name=l25 sig_type=std_logic lab=OUT_M}
